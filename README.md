Carrito Control remoto Web
==================================

Este proyecto está desarrollado en dos capas una de back con spring boot exponiendo funcionalidad por rest, y la capa de presentación en Angular 2 consumiendo el back

El código de back esta :

![Captura de pantalla 2016-11-04 a las 10.14.38 a.m..png](https://bitbucket.org/repo/6b87Kd/images/47120415-Captura%20de%20pantalla%202016-11-04%20a%20las%2010.14.38%20a.m..png)

El código de front está :

![Captura de pantalla 2016-11-04 a las 10.14.49 a.m..png](https://bitbucket.org/repo/6b87Kd/images/2086729471-Captura%20de%20pantalla%202016-11-04%20a%20las%2010.14.49%20a.m..png)

Back
-----
El empaqueta esta con maven,

Para iniciar el back debe correr el comando : mvn spring-boot:run
este estará recibiendo los request por el puerto 9090, el path de los servicios y sus funciones las puede validar y verificar por medio de la interfaz de swagger

http://localhost:9090/swagger-ui.html

![Captura de pantalla 2016-11-04 a las 11.48.33 a.m..png](https://bitbucket.org/repo/6b87Kd/images/349985111-Captura%20de%20pantalla%202016-11-04%20a%20las%2011.48.33%20a.m..png)

![Captura de pantalla 2016-11-04 a las 11.48.44 a.m..png](https://bitbucket.org/repo/6b87Kd/images/3566061372-Captura%20de%20pantalla%202016-11-04%20a%20las%2011.48.44%20a.m..png)

![Captura de pantalla 2016-11-04 a las 11.48.49 a.m..png](https://bitbucket.org/repo/6b87Kd/images/773826773-Captura%20de%20pantalla%202016-11-04%20a%20las%2011.48.49%20a.m..png)

![Captura de pantalla 2016-11-04 a las 11.48.59 a.m..png](https://bitbucket.org/repo/6b87Kd/images/3466413032-Captura%20de%20pantalla%202016-11-04%20a%20las%2011.48.59%20a.m..png)


Front
-----

Primero es installar nodejs y npm
http://blog.npmjs.org/post/85484771375/how-to-install-npm
node -v y npm -v

Después es necesario colocarse sobre el directorio del front
y ejecutar el comando npm install y así instalar todas las dependencias del proyecto

Para iniciar la app front  correr el comando 
ng serve este iniciara la app por el puerto 4200
http://localhost:4200/

![Captura de pantalla 2016-11-04 a las 12.37.03 p.m..png](https://bitbucket.org/repo/6b87Kd/images/3962644072-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.37.03%20p.m..png)

Primer ingreso recoleccion informacion lienzo

![Captura de pantalla 2016-11-04 a las 12.37.56 p.m..png](https://bitbucket.org/repo/6b87Kd/images/3559687534-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.37.56%20p.m..png)

Ingreso de información

![Captura de pantalla 2016-11-04 a las 12.38.24 p.m..png](https://bitbucket.org/repo/6b87Kd/images/1746739529-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.38.24%20p.m..png)

Impresion lienzo

![Captura de pantalla 2016-11-04 a las 12.38.30 p.m..png](https://bitbucket.org/repo/6b87Kd/images/1583872092-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.38.30%20p.m..png)

![Captura de pantalla 2016-11-04 a las 12.38.42 p.m..png](https://bitbucket.org/repo/6b87Kd/images/3165833406-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.38.42%20p.m..png)

Movimiento carro

![Captura de pantalla 2016-11-04 a las 12.41.55 p.m..png](https://bitbucket.org/repo/6b87Kd/images/827220913-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.41.55%20p.m..png)

![Captura de pantalla 2016-11-04 a las 12.42.19 p.m..png](https://bitbucket.org/repo/6b87Kd/images/488272504-Captura%20de%20pantalla%202016-11-04%20a%20las%2012.42.19%20p.m..png)

## Further help

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.17.

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).