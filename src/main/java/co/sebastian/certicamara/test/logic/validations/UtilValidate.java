package co.sebastian.certicamara.test.logic.validations;

import co.sebastian.certicamara.test.logic.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.logic.core.Dashboard;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ssanchez on 1/11/16.
 */
public class UtilValidate {
    private static UtilValidate ourInstance = new UtilValidate();

    public static UtilValidate getInstance() {
        return ourInstance;
    }

    private UtilValidate() {
    }

    public void validateNumberFormat(String n) throws BussinesExcepcion {
        try{
            Integer.valueOf(n);
        }catch (NumberFormatException e){
            throw new BussinesExcepcion("Error en formato de comando");
        }
    }

    public  void validateFormatInput(String movs) throws BussinesExcepcion {
        Pattern p = Pattern.compile("([N|S|O|E],\\d+)(;[N|S|O|E],\\d+)*$");
        Matcher m = p.matcher(movs);
        if (!m.matches()){
            throw new BussinesExcepcion("Error en formato de comando");
        }
    }

    public void validateInstanceDashboard(Dashboard dashboard) throws BussinesExcepcion {
        if(dashboard==null){
            throw new BussinesExcepcion("Error no tiene un lienzo definido");
        }
    }
}
