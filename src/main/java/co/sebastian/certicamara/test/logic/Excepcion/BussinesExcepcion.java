package co.sebastian.certicamara.test.logic.Excepcion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ssanchez on 1/11/16.
 */
@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class BussinesExcepcion extends RestApiException {


    public BussinesExcepcion(String message) {
        super(message);
    }


}
