package co.sebastian.certicamara.test.logic.Excepcion.handlers;


import org.springframework.http.HttpStatus;

import java.util.Objects;

/**
 * Created by Juan on 25/08/2016.
 */

public class CustomErrorInfo {

    private HttpStatus status;
    private String generalMessage;
    private String requestPath;

    private CustomErrorInfo() {
    }

    public static class CustomErrorInfoBuilder {

        private CustomErrorInfo info;

        public CustomErrorInfoBuilder() {
            info = new CustomErrorInfo();
        }

        public CustomErrorInfoBuilder status(HttpStatus status) {
            info.setStatus(status);
            return this;
        }

        public CustomErrorInfoBuilder generalMessage(String generalMessage) {
            info.setGeneralMessage(generalMessage);
            return this;
        }

        public CustomErrorInfoBuilder requestPath(String path) {
            info.setRequestPath(path);
            return this;
        }

        public CustomErrorInfo build() {
            Objects.requireNonNull(info.getStatus(), "the message must have an status");
            Objects.requireNonNull(info.getGeneralMessage());
            return info;
        }

    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getGeneralMessage() {
        return generalMessage;
    }

    public void setGeneralMessage(String generalMessage) {
        this.generalMessage = generalMessage;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }
}
