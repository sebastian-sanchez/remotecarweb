package co.sebastian.certicamara.test.logic.core;


import co.sebastian.certicamara.test.logic.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.logic.constans.Address;
import co.sebastian.certicamara.test.logic.validations.UtilValidate;
import co.sebastian.certicamara.test.models.CarDTO;
import co.sebastian.certicamara.test.models.DashboardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by ssanchez on 2/08/16.
 * <p>
 * <p>
 * Implemtacion de la logica del servicio
 */
@Component
public class ControlCarImp implements ControlCar {

    private Car car;
    private Dashboard dashboard;

    @Autowired
    public ControlCarImp(Car car) {
        this.car = car;
    }


    @Override
    public void setSizeDasboard(DashboardDTO dto) {

        dashboard = new Dashboard(Integer.valueOf(dto.getSizeX()), Integer.valueOf(dto.getSizeY()), car);
    }

    @Override
    public void setMovCar(String movs) throws BussinesExcepcion {


        UtilValidate.getInstance().validateInstanceDashboard(dashboard);
        movs = movs.replaceAll("\"","");

        UtilValidate.getInstance().validateFormatInput(movs);

        Map<Address, Integer> listMov = new HashMap<>();
        StringTokenizer st = new StringTokenizer(movs.toUpperCase(), ";");
        while (st.hasMoreTokens()) {
            String[] mov = st.nextToken().split(",");

            UtilValidate.getInstance().validateNumberFormat(mov[1]);

            listMov.put(Address.fromString(mov[0]), Integer.valueOf(mov[1]));
        }

        for (Map.Entry<Address, Integer> entry : listMov.entrySet()) {
            Address addres = entry.getKey();
            Integer sizeMove = entry.getValue();
            car.moveCar(addres, sizeMove);
        }
    }

    @Override
    public CarDTO getCurrentPosicionCar() throws BussinesExcepcion {
        UtilValidate.getInstance().validateInstanceDashboard(dashboard);
        dashboard.validateLimitsSize();
        return new CarDTO(car.getCurrentPositionX(), car.getCurrentPositionY());
    }
}
