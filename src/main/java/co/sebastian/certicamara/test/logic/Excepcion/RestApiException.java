package co.sebastian.certicamara.test.logic.Excepcion;

/**
 * Created by Juan on 26/08/2016.
 */
public class RestApiException extends RuntimeException {

    public RestApiException(String message) {
        super(message);
    }

    public RestApiException(String message, Throwable t) {
        super(message, t);
    }
}
