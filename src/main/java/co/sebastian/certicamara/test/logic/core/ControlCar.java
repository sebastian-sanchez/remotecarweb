package co.sebastian.certicamara.test.logic.core;

import co.sebastian.certicamara.test.logic.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.models.CarDTO;
import co.sebastian.certicamara.test.models.DashboardDTO;

/**
 * Created by ssanchez on 8/08/16.
 *
 * Iterfas facha de la logica del servicios
 */
public interface ControlCar {


    void setSizeDasboard(DashboardDTO dto);

    void setMovCar(String mov) throws BussinesExcepcion;

    CarDTO getCurrentPosicionCar() throws BussinesExcepcion;
}
