package co.sebastian.certicamara.test.logic.Excepcion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ssanchez on 2/11/16.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RestApiException {

    public BadRequestException(String message, Throwable t) {
        super(message, t);
    }

    public BadRequestException(String message) {
        super(message);
    }
}
