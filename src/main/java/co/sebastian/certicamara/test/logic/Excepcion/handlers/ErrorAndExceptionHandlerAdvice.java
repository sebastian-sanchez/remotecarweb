package co.sebastian.certicamara.test.logic.Excepcion.handlers;

import co.sebastian.certicamara.test.logic.Excepcion.RestApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Juan on 25/08/2016.
 */
@ControllerAdvice(annotations = RestController.class)
public class ErrorAndExceptionHandlerAdvice {

    private final Logger log = LoggerFactory.getLogger(ErrorAndExceptionHandlerAdvice.class);

    @ExceptionHandler(RestApiException.class)
    @ResponseBody
    CustomErrorInfo handleBadRestApiException(HttpServletRequest req, RestApiException restex) {
        log.error(restex.getMessage(), (Object) restex.getStackTrace());
        return
                new CustomErrorInfo.CustomErrorInfoBuilder()
                        .status(AnnotationUtils.findAnnotation(restex.getClass(), ResponseStatus.class).value())
                        .generalMessage(restex.getMessage())
                        .requestPath(req.getRequestURI())
                        .build();
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    CustomErrorInfo handleRuntimeException(HttpServletRequest req, RuntimeException rex) {
        log.error(rex.getMessage(), (Object) rex.getStackTrace());
        String exceptionName = rex.getCause() != null ? rex.getCause().toString() : rex.toString();
        return
                new CustomErrorInfo.CustomErrorInfoBuilder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .generalMessage("The: " + exceptionName + ", was thrown by the server when processing the request")
                        .requestPath(req.getRequestURI())
                        .build();
    }
}
