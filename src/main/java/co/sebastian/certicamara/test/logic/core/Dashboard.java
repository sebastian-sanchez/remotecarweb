package co.sebastian.certicamara.test.logic.core;

import co.sebastian.certicamara.test.logic.Excepcion.BussinesExcepcion;
import org.springframework.stereotype.Component;

/**
 * Created by ssanchez on 31/10/16.
 */
public class Dashboard {

    private int sizeX;
    private int sizeY;

    private Car car;

    public Dashboard(int sizeX, int sizeY, Car car) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.car = car;
    }


    public void validateLimitsSize() throws BussinesExcepcion {
        if (sizeX < car.getCurrentPositionX() || 0 > car.getCurrentPositionX() || sizeY < car.getCurrentPositionY() || 0 > car.getCurrentPositionY()) {
            car.revertMoveCar();
            throw new BussinesExcepcion("Se ha detenido el avance por salir de los límites");
        }
    }


}
