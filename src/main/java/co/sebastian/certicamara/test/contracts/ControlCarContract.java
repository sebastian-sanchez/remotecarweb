package co.sebastian.certicamara.test.contracts;


import co.sebastian.certicamara.test.logic.Excepcion.BadRequestException;
import co.sebastian.certicamara.test.logic.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.logic.core.ControlCar;
import co.sebastian.certicamara.test.models.CarDTO;
import co.sebastian.certicamara.test.models.DashboardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ssanchez on 2/08/16.
 * <p>
 * <p>
 * contrato de exposiion del servicio
 */

@RestController
@RequestMapping(ControlCarContractDoc.API) // path exposicion
@CrossOrigin
public class ControlCarContract implements ControlCarContractDoc {

    @Autowired
    private ControlCar service;


    @RequestMapping(path = "/dashboard", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void setSizeDasboard(@RequestBody DashboardDTO dto) {

        if (dto.getSizeX() == null || dto.getSizeY() == null) {
            throw new BadRequestException("Area must not have an SIZE X AND SIZE Y");
        }
        service.setSizeDasboard(dto);

    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void setMovCar(@RequestBody String mov) throws BussinesExcepcion {

        if (mov == null) {
            throw new BadRequestException("Area must not have any");
        }

        service.setMovCar(mov);


    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CarDTO> getCurrentPosicionCar() throws BussinesExcepcion {
        CarDTO carDTO = service.getCurrentPosicionCar();
        return ResponseEntity.ok().body(carDTO);
    }
}
