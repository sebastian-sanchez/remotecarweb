package co.sebastian.certicamara.test.contracts;

import co.sebastian.certicamara.test.logic.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.models.CarDTO;
import co.sebastian.certicamara.test.models.DashboardDTO;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;

/**
 * Created by ssanchez on 12/08/16.
 */
@Api(value = ControlCarContractDoc.API, description = "Api de expocicion de controlador de carrito")
public interface ControlCarContractDoc {

    String API = "/v1/controllCar";

    @ApiOperation(value = "setSizeDasboard", nickname = "setSizeDasboard") // documentacion de la operacion expuesta
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Failure")})
        // documentacion de repuesta del servicios
    void setSizeDasboard(@ApiParam(value = "tamaño lienzo", required = true) DashboardDTO dto);

    @ApiOperation(value = "setMovCar", nickname = "setMovCar") // documentacion de la operacion expuesta
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Failure")})
        // documentacion de repuesta del servicios
   void setMovCar(String mov) throws BussinesExcepcion;


    @ApiOperation(value = "getCurrentPosicionCar", nickname = "getCurrentPosicionCar",
            response = CarDTO.class) // documentacion de la operacion expuesta
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Failure")})
        // documentacion de repuesta del servicios
    ResponseEntity<CarDTO> getCurrentPosicionCar() throws BussinesExcepcion;
}
