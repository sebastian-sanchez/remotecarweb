package co.sebastian.certicamara.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
public class CrontrolCarWebApplication {


    public static void main(String[] args) {
        SpringApplication.run(CrontrolCarWebApplication.class, args);
    }


    /**
     * Metodo para la generacion de metada del servicios y exponerlar en json por el path /v2/api-docs
     *
     * @return
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(metadata())
                .pathMapping("/")
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/v1/.*"))
                .build();
    }

    /**
     * Informacion general sobre el servicio expuesto
     *
     * @return
     */
    public ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Consumos de servicios rest")
                .description("prueba certicarma")
                .version("1.1")
                .termsOfServiceUrl("")
                .contact(new Contact("JUAN SEBASTIAN SANCHEZ", "", "SEBASTIANSANCHEZ88@GMAIL.COM"))
                .build();
    }
}
