package co.sebastian.certicamara.test.models;

/**
 * Created by ssanchez on 2/11/16.
 */
public class DashboardDTO {

    private String sizeX;
    private String sizeY;

    public String getSizeX() {
        return sizeX;
    }

    public void setSizeX(String sizeX) {
        this.sizeX = sizeX;
    }

    public String getSizeY() {
        return sizeY;
    }

    public void setSizeY(String sizeY) {
        this.sizeY = sizeY;
    }
}
