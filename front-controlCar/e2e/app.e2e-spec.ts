import { FrontControlCarPage } from './app.po';

describe('front-control-car App', function() {
  let page: FrontControlCarPage;

  beforeEach(() => {
    page = new FrontControlCarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
