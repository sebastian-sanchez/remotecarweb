import {Component} from "@angular/core";
import {DashboardServiceService} from "./shared/services/dashboard-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Carrito Control remoto';

  showConfigLienzo: boolean = true;

  sizeY: number = 0;
  sizeX: number = 0;

  pocicionCarY: number = 0;
  pocicionCarX: number = 0;

  constructor(private dashboardServiceService: DashboardServiceService) {
  }


  setConfigLienzo(event) {

    this.dashboardServiceService.setDasboard(event).subscribe(
      comments => {
// Emit list event
        this.showConfigLienzo = false;
        this.sizeY = event.sizeY;
        this.sizeX = event.sizeX;
        console.log("---> " + this.sizeX);
        console.log("---> " + this.sizeY);
      },
      err => {
// Log errors if any
        console.log(err);
      });
  }

  setMovLienzo(event) {


    this.dashboardServiceService.setMovcar(event).subscribe(
      comments => {
        console.log("---> " + this.pocicionCarY);
        console.log("---> " + this.pocicionCarX);
      },
      err => {

        console.log(err);
      });


    this.dashboardServiceService.getCurrentPocicion().subscribe(
      comments => {
        console.log(comments)
        this.pocicionCarX=comments.currentPositionX;
        this.pocicionCarY=comments.currentPositionY;
      },
      err => {

        console.log(err);
      });

  }


}
