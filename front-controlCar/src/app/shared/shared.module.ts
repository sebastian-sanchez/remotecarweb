import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {LienzoComponent} from "./lienzo/lienzo.component";
import {PipePipe} from "./pipe/pipe.pipe";
import {InputLienzoComponent} from "./input-lienzo/input-lienzo.component";
import {InputMovComponent} from "./input-mov/input-mov.component";
import {DashboardServiceService} from "./services/dashboard-service.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule
  ],
  declarations: [LienzoComponent, PipePipe, InputLienzoComponent, InputMovComponent],
  exports: [LienzoComponent, InputLienzoComponent, InputMovComponent],
  providers: [
    DashboardServiceService
  ]
})
export class SharedModule {
}
