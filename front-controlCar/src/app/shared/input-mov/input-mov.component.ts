import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-input-mov',
  templateUrl: './input-mov.component.html',
  styleUrls: ['./input-mov.component.css']
})
export class InputMovComponent implements OnInit {

  movNext:string;


  @Output() submitMov = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

  public doNextMov(event):void {
    let movs = (<HTMLInputElement>event.target).value;

    console.log(" -  whatttt - " +movs);
    this.submitMov.emit(movs);
  }

}
