import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class PipePipe implements PipeTransform {

  transform(arr) {
    var copy = arr.slice();
    return copy.reverse();
  }
}
