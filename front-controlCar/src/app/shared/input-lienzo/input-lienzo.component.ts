import {Component, OnInit, Output, EventEmitter} from "@angular/core";
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";


@Component({
  selector: 'app-input-lienzo',
  templateUrl: './input-lienzo.component.html',
  styleUrls: ['./input-lienzo.component.css']
})
export class InputLienzoComponent implements OnInit {

  form: FormGroup;

  submitted = false;

  @Output() submitConf = new EventEmitter();

  constructor(_fb: FormBuilder) {

  }

  ngOnInit() {

    this.form = new FormGroup({
      sizeX: new FormControl('', [<any>Validators.required, <any>Validators.pattern("[0-9]*")]),
      sizeY: new FormControl('', [<any>Validators.required, <any>Validators.pattern("[0-9]*")])
    })
    ;

  }

  save(model: any, isValid: boolean) {


    console.log(model, isValid);
    this.submitConf.emit(model);
  }
}
