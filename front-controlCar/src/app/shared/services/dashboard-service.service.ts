import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

// Import RxJs required methods


@Injectable()
export class DashboardServiceService {

  constructor(private http: Http) {
  }

  private dashboardConfUrl = 'http://localhost:9090/v1/controllCar/dashboard';
  private carUrl = 'http://localhost:9090/v1/controllCar';

  getCurrentPocicion(): Observable<any> {

    // ...using get request
    return this.http.get(this.carUrl)
    // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  setDasboard(body: Object): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    console.log(bodyString);
    let headers = new Headers({'Content-Type': 'application/json'}); // ... Set content type to JSON
    let options = new RequestOptions({headers: headers}); // Create a request option

    return this.http.post(this.dashboardConfUrl, bodyString, options) // ...using post request
          // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(console.error(error) || 'Server error')); //...errors if any
  }

  setMovcar(body: Object): Observable<any> {
    let bodyString = JSON.stringify(body); // Stringify payload
    console.log(bodyString);
    let headers = new Headers({'Content-Type': 'application/json'}); // ... Set content type to JSON
    let options = new RequestOptions({headers: headers}); // Create a request option

    return this.http.post(this.carUrl, bodyString, options) // ...using post request
      .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
      .catch((error: any) => Observable.throw(console.error(error) || ' hphphph Server error')); //...errors if any
  }


}
